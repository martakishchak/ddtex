package utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestDataProvider {

    public static List<TestDataType> getDataFromJson(String fileName) throws IOException {

        final ObjectMapper objectMapper = new ObjectMapper();
        List<TestDataType> langList = objectMapper.readValue(
                new File(fileName),
                new TypeReference<List<TestDataType>>(){});

        //    langList.forEach(x -> System.out.println(x.toString()));
        return langList;
    }
}
