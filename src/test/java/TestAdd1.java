import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.loader.LoaderType;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.testng.Assert;

@RunWith(DataDrivenTestRunner.class)
public class TestAdd1 {
    @Test
    @DataLoader(filePaths = "src/main/resources/testData.xls", loaderType = LoaderType.EXCEL, writeData = false)
    public void test2(@Param(name="a")int a, @Param(name="b")int b, @Param(name="sum")int sum){
        System.out.println(a + " + " + b + " = " + sum);
        Assert.assertEquals(Mathem.add(a, b), sum);
    }

    @Test
    @DataLoader(filePaths = "src/main/resources/testAddData.csv", loaderType = LoaderType.CSV, writeData = false)
    public void test3(@Param(name="a")int a, @Param(name="b")int b, @Param(name="sum")int sum){
        System.out.println(a + " + " + b + " = " + sum);
        Assert.assertEquals(Mathem.add(a, b), sum);
    }
}
