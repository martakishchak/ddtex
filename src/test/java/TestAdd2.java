import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.TestDataProvider;
import utils.TestDataType;

import java.io.IOException;
import java.util.List;

public class TestAdd2 {

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod() throws IOException {
        List<TestDataType> resList = TestDataProvider.getDataFromJson("src/main/resources/testData.json");
        int columnsCount = 3;
        Object[][] result = new Object[resList.size()][columnsCount];
        for (int i = 0; i < resList.size(); i++) {
            result[i][0] = resList.get(i).getA();
            result[i][1] = resList.get(i).getB();
            result[i][2] = resList.get(i).getSum();
        }
        return result;
    }

    @Test(dataProvider = "data-provider")
    public void test4(int a, int b, int sum){
        System.out.println(a + " + " + b + " = " + sum);
        Assert.assertEquals(Mathem.add(a, b), sum);
    }
}
