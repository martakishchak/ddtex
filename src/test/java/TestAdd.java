import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.testng.Assert;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class TestAdd {
    private int a;
    private int b;
    private int summ;

    @Parameterized.Parameters
    public static Collection testData() {
        return Arrays.asList(
                new Object[][]{{1, 2, 3}, {5, 6, 11},
                }
        );
    }

    public TestAdd(int a, int b, int summ){
        this.a = a;
        this.b = b;
        this.summ = summ;
    }

    @Test
    public void test1(){
        Assert.assertEquals(Mathem.add(a, b), summ);
    }
}
